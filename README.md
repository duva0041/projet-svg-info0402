<img align="left" width="80" height="80" src="Compte-rendu/images/logo.png" alt="Logo">

# Projet SVG - INFO0402

<hr>

Projet consistant en la réalisation d'une interface pour édtier des images vectorielles (SVG).

<hr>

Réalisé par Antoine Duval et Antoine Théologien (S4F3).

<img src="Compte-rendu/images/exemple_formes.png" alt="Exemple de canevas">
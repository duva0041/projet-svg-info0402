#ifndef _EDITEUR_H_
#define _EDITEUR_H_

#include <stdio.h>
#include <string.h>
#include "GeomBase.h"
#include "Surface.h"
#include "Curve.h"
#include "Figure.h"

void taille_image(int *hauteur, int *largeur, float *ratio_hauteur, float *ratio_largeur, int etat);

void transformation_majucule(char *str);

float max(float a, float b);

void sauvegarde_forme(Figure& fig, Figure** tab_fig, int* taille_tab);

void choix_couleur(Color *couleur);
void choix_centre(float* x, float* y, int* hauteur, int* largeur);
void choix_coin(float* x, float* y, int* hauteur, int* largeur, float* longueur_x, float* longueur_y);
void choix_largeur(float* largeur);
void choix_rayon(float* rayon);
void choix_rayon_(float* rayon_x, float* rayon_y);
void choix_longueur(float* longueur);
void choix_angle(float* angle);
void choix_hauteur(float* hauteur);
void choix_epaisseur(size_t* epaisseur);
void choix_nombre_points(int* nombre_points, int forme);
void choix_point(float* x, float* y, int* hauteur, int* largeur, int numero);

void ajout_forme(Figure& fig, Figure** tab_fig, int* taille_tab, int* hauteur, int* largeur);
void selection_forme(Figure& fig, int* hauteur, int* largeur, int* taille, int* tableau);
void supprimer_forme(Figure& fig, int* hauteur, int* largeur, int* taille, int* tableau);
void modifier_forme(Figure& fig, int* hauteur, int* largeur);

int selection_type(int etat);

void menu_principal(Figure& fig, char* nom_du_fichier, int *hauteur, int *largeur, float *ratio_hauteur, float *ratio_largeur, Figure** tab_fig, int *taille_tab_fig);


#endif // _EDITEUR_H_
#ifndef _SURFACE_H_
#define _SURFACE_H_
#include <iostream>
#include <cassert>
#include "GeomBase.h"
#include "SVG.h"
#include "Array.h"

namespace Geom2D {
	namespace Surface {
		////////////////////////////////////////////////////////////////////////////////////////
		// interface FillProperty
		class Fill {
		protected:
			Color   fill_color;
		public:
			explicit Fill(const Color &c = Color::defaut) : fill_color(c) {}
			void setFillColor(const Color &color) { fill_color = color; }
			const Color &getFillColor() const { return fill_color; }
			friend std::ostream& operator<<(std::ostream& s, const Fill& d);
			friend SVG::stream& operator<<(SVG::stream& s, const Fill& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Surface
		class Base :
			public Geom2D::Base,
			public Fill {
		protected:
		public:
			Base() : Geom2D::Base(), Fill() {}
			Base(const Color &color) : Fill(color) {}
			Base(const Point &, const Color &col = Color::defaut) : Fill(col) {}	// le premier paramètre est inutilisé, pas besoin de le nommer.
			virtual ~Base() {}
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Disk
		class Disk :
			public Surface::Base {
		protected:
			Point center;
			float radius;
		public:
			Disk(const Point &xy, float r, const Color &c = Color::defaut) :
				Base(xy,c), center(xy), radius(r) {}
			Disk() : Disk({0,0}, 0.0f, Color::defaut) {}
			virtual ~Disk() {}
			virtual std::ostream &info(std::ostream &stream) const;
			virtual void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) override { center += v; }
			void Scale(float s) override { radius *= s; }
			void Resize(float s) override { radius *= s; }
			Point Center() { return center; }
			void Rotate(float) override {}	// ne rien faire : omission du nom du paramètre (évite un warning "unused parameter")
			int getType() { return 5; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Disk& d);
			friend SVG::stream& operator<<(SVG::stream& s, const Disk& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Oval
		class Oval :
			public Surface::Base {
		protected:
			Point center;
			float a, b, angle;
		public:
			Oval(const Point &xy, float _a, float _b, const Color &c = Color::defaut) :
				Base(xy,c), center(xy), a(_a), b(_b) { angle = 0.0f; }
			virtual ~Oval() {}
			virtual std::ostream &info(std::ostream &stream) const;
			virtual void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) override { center += v; }
			void Scale(float s) override { a *= s; b *= s; }
			void Resize(float s) override { a *= s; b *= s; }
			Point Center() { return center; }
			void Rotate(float ang) override { angle = ang; }
			int getType() { return 6; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Oval& d);
			friend SVG::stream& operator<<(SVG::stream& s, const Oval& p);
		};


		////////////////////////////////////////////////////////////////////////////////////////
		//  Polygon
		class Polygon : public Surface::Base, public Array::Fixed<Point> {
		public:
			Polygon(std::size_t npoints, const Point &centre, float size, const Color &c = Color::defaut);
			Polygon(const std::initializer_list<Point> &list, const Color &c = Color::defaut);
			virtual ~Polygon() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s)            { Array::Geometry::Scale(*this,s); }
			void Resize(float s)            { Array::Geometry::Resize(*this,s); }
			void Rotate(float a)           { Array::Geometry::Rotate(*this,a); }
			Point Center() { return Array::Geometry::Center(*this); }
			int getType() { return 7; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Polygon& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Polygon& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Triangle
		class Triangle : public Surface::Base, public Array::Static<Point,3> {
		public:
			// `cent` est le centre du triangle et `size` la distance entre les sommets et le centre.
			Triangle(const Point &cent, float size, const Color &col = Color::defaut) :
				Base(cent,col), Array::Static<Point,3>()
				{ Array::Geometry::FillnGon(*this,cent,size); }
			Triangle(const std::initializer_list<Point> &list, const Color &col = Color::defaut) :
				Base(col), Array::Static<Point,3>(list) {}
			virtual ~Triangle() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s) { Array::Geometry::Scale(*this,s); }
			void Resize(float s) { Array::Geometry::Resize(*this,s); }
			void Rotate(float a) { Array::Geometry::Rotate(*this,a); }
			Point Center() { return Array::Geometry::Center(*this); }
			int getType() { return 8; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Triangle& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Triangle& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Pentagon
		class Pentagon : public Surface::Base, public Array::Static<Point,5> {
		public:
			// `cent` est le centre du pentagon et `size` la distance entre les sommets et le centre.
			Pentagon(const Point &cent, float size, const Color &col = Color::defaut) :
				Base(cent,col), Array::Static<Point,5>()
				{ Array::Geometry::FillnGon(*this,cent,size); }
			Pentagon(const std::initializer_list<Point> &list, const Color &col = Color::defaut) :
				Base(col), Array::Static<Point,5>(list) {}
			virtual ~Pentagon() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s) { Array::Geometry::Scale(*this,s); }
			void Resize(float s) { Array::Geometry::Resize(*this,s); }
			void Rotate(float a) { Array::Geometry::Rotate(*this,a); }
			Point Center() { return Array::Geometry::Center(*this); }
			int getType() { return 9; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Pentagon& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Pentagon& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Hexagon
		class Hexagon : public Surface::Base, public Array::Static<Point,6> {
		public:
			// `cent` est le centre du triangle et `size` la distance entre les sommets et le centre.
			Hexagon(const Point &cent, float size, const Color &col = Color::defaut) :
				Base(cent,col), Array::Static<Point,6>()
				{ Array::Geometry::FillnGon(*this,cent,size); }
			Hexagon(const std::initializer_list<Point> &list, const Color &col = Color::defaut) :
				Base(col), Array::Static<Point,6>(list) {}
			virtual ~Hexagon() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			Point Center() { return Array::Geometry::Center(*this); }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s) { Array::Geometry::Scale(*this,s); }
			void Resize(float s) { Array::Geometry::Resize(*this,s); }
			void Rotate(float a) { Array::Geometry::Rotate(*this,a); }
			int getType() { return 10; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Hexagon& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Hexagon& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Heptagon
		class Heptagon : public Surface::Base, public Array::Static<Point,7> {
		public:
			// `cent` est le centre du triangle et `size` la distance entre les sommets et le centre.
			Heptagon(const Point &cent, float size, const Color &col = Color::defaut) :
				Base(cent,col), Array::Static<Point,7>()
				{ Array::Geometry::FillnGon(*this,cent,size); }
			Heptagon(const std::initializer_list<Point> &list, const Color &col = Color::defaut) :
				Base(col), Array::Static<Point,7>(list) {}
			virtual ~Heptagon() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			Point Center() { return Array::Geometry::Center(*this); }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s) { Array::Geometry::Scale(*this,s); }
			void Resize(float s) { Array::Geometry::Resize(*this,s); }
			void Rotate(float a) { Array::Geometry::Rotate(*this,a); }
			int getType() { return 11; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Heptagon& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Heptagon& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Octogon
		class Octogon : public Surface::Base, public Array::Static<Point,8> {
		public:
			// `cent` est le centre de l'octogone et `size` la distance entre les sommets et le centre.
			Octogon(const Point &cent, float size, const Color &col = Color::defaut) :
				Base(cent,col), Array::Static<Point,8>()
				{ Array::Geometry::FillnGon(*this,cent,size); }
			Octogon(const std::initializer_list<Point> &list, const Color &col = Color::defaut) :
				Base(col), Array::Static<Point,8>(list) {}
			virtual ~Octogon() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			Point Center() {return Array::Geometry::Center(*this); }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s) { Array::Geometry::Scale(*this,s); }
			void Resize(float s) { Array::Geometry::Resize(*this,s); }
			void Rotate(float a) { Array::Geometry::Rotate(*this,a); }
			int getType() { return 12; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Octogon& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Octogon& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		//  Rectangle
		class Rectangle : public Surface::Base {
		protected:
			Point corner;
			float width, height;
			float rotate;
		public:
			Rectangle(const Point &xy, float w, float h, const Color &c)
				: Base(c), corner(xy), width(w), height(h), rotate(0.f) {}
			virtual ~Rectangle() {};
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { corner += v; }
			void Scale(float s) { width *= s; height *= s; }
			void Resize(float s) { width *= s; height *= s; }
			void Rotate(float a) { rotate += a; }
			Rectangle *real_type() { return this; }
			Point Center() { return corner; }
			int getType() { return 13; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Rectangle& d);
			friend SVG::stream& operator<<(SVG::stream& s, const Rectangle& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		///  Square
		class Square : public Rectangle {
		public:
			Square(const Point &xy, float s, const Color &c)
				: Rectangle(xy, s, s, c) {}
			virtual ~Square() {};
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			int getType() { return 14; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Square& d);
			friend SVG::stream& operator<<(SVG::stream& s, const Square& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		///  Losange
		class Losange : public Surface::Base, public Array::Static<Point,4> {
		protected:
			Point center;
			float diagonal1, diagonal2;
		public:
			Losange(const Point &c, float diag1, float diag2, const Color &col = Color::defaut) :
				Base(c,col), Array::Static<Point,4>()
				{ 
					this->center = c;
					this->diagonal1 = diag1;
					this->diagonal2 = diag2;
					Array::Geometry::FillLosange(*this, c, diag1, diag2); 
				}
			Losange(const std::initializer_list<Point> &list, const Color &col = Color::defaut) :
				Base(col), Array::Static<Point,4>(list) {}
			virtual ~Losange() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s) { Array::Geometry::Scale(*this,s); }
			void Resize(float s) { Array::Geometry::Resize(*this,s); }
			void Rotate(float a) { Array::Geometry::Rotate(*this,a); }
			Point Center() { return center; }
			int getType() { return 15; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Losange& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Losange& p);
		};

		////////////////////////////////////////////////////////////////////////////////////////
		///  Parallelogram
		class Parallelogramme : public Surface::Base, public Array::Static<Point, 4> {
		protected:
			Point center;
			float width, height, angle;
		public:
			Parallelogramme(const Point& c, float w, float h, float a, const Color& col = Color::defaut) :
				Base(c, col), Array::Static<Point, 4>(),
				center(c), width(w), height(h), angle(a)
			{
				Array::Geometry::FillParallelogramme(*this, c, w, h, a);
			}
			Parallelogramme(const std::initializer_list<Point>& list, const Color& col = Color::defaut) :
				Base(col), Array::Static<Point, 4>(list) {}

			virtual ~Parallelogramme() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream& stream) const { stream << *this; }
			void Translate(const Point& v) { Array::Geometry::Translate(*this, v); }
			void Scale(float s) { Array::Geometry::Scale(*this, s); }
			void Resize(float s) { Array::Geometry::Resize(*this, s); }
			void Rotate(float a) { Array::Geometry::Rotate(*this, a); }
			Point Center() { return center; }
			int getType() { return 16; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Parallelogramme& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Parallelogramme& p);
		};



		//////////////////////////////////////////////////////////////////////////////////////////
		// Étoile
		class Etoile : public Surface::Base, public Array::Static<Point, 10> {
		public:
			// `cent` est le centre de l'étoile et `size` la distance entre le centre et les sommets
			Etoile(const Point& cent, float size, const Color& col = Color::defaut) :
				Base(cent, col), Array::Static<Point, 10>()
				{ Array::Geometry::FillStar(*this, cent, size); }
			virtual ~Etoile() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s)            { Array::Geometry::Scale(*this,s); }
			void Resize(float s)            { Array::Geometry::Resize(*this,s); }
			void Rotate(float a)           { Array::Geometry::Rotate(*this,a); }
			Point Center() { return Array::Geometry::Center(*this); }
			int getType() { return 17; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Etoile& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Etoile& p);
		};


		//////////////////////////////////////////////////////////////////////////////////////////
		// Sun
		class Sun : public Surface::Base, public Array::Static<Point, 36> {
		public:
			// `cent` est le centre de l'étoile et `size` la distance entre le centre et les sommets
			Sun(const Point& cent, float size, const Color& col = Color::defaut) :
				Base(cent, col), Array::Static<Point, 36>()
				{ Array::Geometry::FillSun(*this, cent, size); }
			virtual ~Sun() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s)            { Array::Geometry::Scale(*this,s); }
			void Resize(float s)            { Array::Geometry::Resize(*this,s); }
			void Rotate(float a)           { Array::Geometry::Rotate(*this,a); }
			Point Center() { return Array::Geometry::Center(*this); }
			int getType() { return 18; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const Sun& p);
			friend SVG::stream& operator<<(SVG::stream& s, const Sun& p);
		};


		//////////////////////////////////////////////////////////////////////////////////////////
		// SnowFlake
		class SnowFlake : public Surface::Base, public Array::Static<Point, 12> {
		public:
			// `cent` est le centre de l'étoile et `size` la distance entre le centre et les sommets
			SnowFlake(const Point& cent, float size, const Color& col = Color::defaut) :
				Base(cent, col), Array::Static<Point, 12>()
				{ Array::Geometry::FillSnowFlake(*this, cent, size); }
			virtual ~SnowFlake() {}
			std::ostream& info(std::ostream& stream) const;
			void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) { Array::Geometry::Translate(*this,v); }
			void Scale(float s)            { Array::Geometry::Scale(*this,s); }
			void Resize(float s)            { Array::Geometry::Resize(*this,s); }
			void Rotate(float a)           { Array::Geometry::Rotate(*this,a); }
			Point Center() { return Array::Geometry::Center(*this); }
			int getType() { return 19; }
			void setColor(Color couleur) override { setFillColor(couleur); }
			friend std::ostream& operator<<(std::ostream& s, const SnowFlake& p);
			friend SVG::stream& operator<<(SVG::stream& s, const SnowFlake& p);
		};


		//////////////////////////////////////////////////////////////////////////////////////////
		// Coeur
		class Coeur : public Surface::Base {
		protected:
			Disk disk1, disk2;
			Triangle triangle;
		public:
		Coeur(const Point &center, float size, const Color &color = Color::defaut) :
		Base(center, color), 
		disk1({center.getx()-size/3, center.gety()}, size/2, color),
		disk2({center.getx() + size/3, center.gety()}, size/2, color),
		triangle({{center.getx()-(3*size/4), center.gety()+size/4}, 
			{center.getx()+(3*size/4), center.gety()+size/4}, 
			{center.getx(), center.gety()+size}}, color) {
		}
		virtual ~Coeur() {}
		virtual std::ostream &info(std::ostream &stream) const;
		virtual void write(SVG::stream &stream) const { stream << *this; }
		void Translate(const Point &v) override { disk1.Translate(v); disk2.Translate(v); triangle.Translate(v); }
		void Scale(float s) override { disk1.Scale(s); disk2.Scale(s); triangle.Scale(s); }
		void Resize(float s) override { disk1.Resize(s); disk2.Resize(s); triangle.Resize(s); }
		void Rotate(float a) override { disk1.Rotate(a); disk2.Rotate(a); triangle.Rotate(a); }
		Point Center() { 
			return 
				{
				(disk1.Center().getx() + disk2.Center().getx() + triangle.Center().getx())/3,
				(disk1.Center().gety() + disk2.Center().gety() + triangle.Center().gety())/3
				};
			  }
		int getType() { return 20; }
		void setColor(Color couleur) override { disk1.setColor(couleur); disk2.setColor(couleur); triangle.setColor(couleur); }
		friend std::ostream& operator<<(std::ostream& s, const Coeur& c);
		friend SVG::stream& operator<<(SVG::stream& s, const Coeur& c);
		};



		//////////////////////////////////////////////////////////////////////////////////////////
		// Flower
		class Flower : public Surface::Base {
		protected:
			Disk disks[7];  // Tableau de 6 disques pour former une fleur
		public:
			Flower(const Point &center, float radius, const Color &color = Color::defaut) :
				Base(center, color) {
				float angle = 0;
				float step = 3.14159265358979323846f / 3;  // Angle entre chaque disque
				for (int i = 0; i < 6; i++) {
					// Calcul des coordonnées du centre du disque
					float x = static_cast<float>(center.getx() + radius/2.0f * cos(angle));
					float y = static_cast<float>(center.gety() + radius/2.0f * sin(angle));
					Point diskCenter(x, y);
					disks[i] = Disk(diskCenter, radius/3, color);
					angle += step;
				}
				disks[6] = Disk({center.getx(), center.gety()}, radius/3, Color::yellow);
			}
			virtual ~Flower() {}
			virtual std::ostream &info(std::ostream &stream) const;
			virtual void write(SVG::stream &stream) const { stream << *this; }
			void Translate(const Point &v) override {
				for (int i = 0; i < 6; i++) {
					disks[i].Translate(v);
				}
			}
			void Scale(float s) override {
				for (int i = 0; i < 6; i++) {
					disks[i].Scale(s);
				}
			}
			void Resize(float s) override {
				for (int i = 0; i < 6; i++) {
					disks[i].Resize(s);
				}
			}
			Point Center() override { return disks[0].Center(); }
			void Rotate(float angle) override {
				for (int i = 0; i < 6; i++) {
					disks[i].Rotate(angle);
				}
			}
			int getType() override { return 21;	}
			void setColor(Color color) override {
				for (int i = 0; i < 6; i++) {
					disks[i].setColor(color);
				}
			}
			friend std::ostream& operator<<(std::ostream& s, const Flower& c);
			friend SVG::stream& operator<<(SVG::stream& s, const Flower& c);
		};

		


		//////////////////////////////////////////////////////////////////////////////////////////
		// Pikachu
		class Pikachu : public Surface::Base {
		protected:
			Point center;
			Disk tete, oeil1, oeil2, oeil3, oeil4, joue1, joue2, nez;
			Oval bouche, oreille1, oreille2;
		public:
		Pikachu(const Point &center, float size) :
			Base(center, Color::yellow), center(center),
			
			tete(center, size, Color::yellow),

			oeil1({center.getx()+(size*0.40f), center.gety()-(size*0.2f)}, size/5, Color::black),
			oeil2({center.getx()-(size*0.40f), center.gety()-(size*0.2f)}, size/5, Color::black),

			oeil3({center.getx()+(size*0.35f), center.gety()-(size*0.25f)}, size/10, Color::white),
			oeil4({center.getx()-(size*0.35f), center.gety()-(size*0.25f)}, size/10, Color::white),

			joue1({center.getx()+(size*0.6f), center.gety()+(size*0.35f)}, size/4, Color::red),
			joue2({center.getx()-(size*0.6f), center.gety()+(size*0.35f)}, size/4, Color::red),

			
			nez({center.getx(), center.gety()}, size/30, Color::black),
			
			bouche({center.getx(), center.gety()+(size*0.5f)}, size/6, size/3, Color::pink),

			oreille1({center.getx()+(size*0.95f), center.gety()-(size)}, size*0.6f, size/4, Color::yellow),
			oreille2({center.getx()-(size*0.95f), center.gety()-(size)}, size*0.6f, size/4, Color::yellow)

			{
				oreille1.Rotate(305.0f);
				oreille2.Rotate(55.0f);
			}

		virtual ~Pikachu() {}
		virtual std::ostream &info(std::ostream &stream) const;
		virtual void write(SVG::stream &stream) const { stream << *this; }
		void Translate(const Point &v) override { 
			tete.Translate(v);
			oeil1.Translate(v);
			oeil2.Translate(v);
			oeil3.Translate(v);
			oeil4.Translate(v);
			nez.Translate(v);
			bouche.Translate(v);
			joue1.Translate(v);
			joue2.Translate(v);
			oreille1.Translate(v);
			oreille2.Translate(v);

		}
		void Scale(float s) override { 
			tete.Scale(s);
			oeil1.Scale(s);
			oeil2.Scale(s);
			oeil3.Scale(s);
			oeil4.Scale(s);
			nez.Scale(s);
			bouche.Scale(s);
			joue1.Scale(s);
			joue2.Scale(s);
			oreille1.Scale(s);
			oreille2.Scale(s);

		}
		void Resize(float s) override { 
			tete.Resize(s);
			oeil1.Resize(s);
			oeil2.Resize(s);
			oeil3.Resize(s);
			oeil4.Resize(s);
			nez.Resize(s);
			bouche.Resize(s);
			joue1.Resize(s);
			joue2.Resize(s);
			oreille1.Resize(s);
			oreille2.Resize(s);

		}
		void Rotate(float a) override { 
			tete.Rotate(a);
			oeil1.Rotate(a);
			oeil2.Rotate(a);
			oeil3.Rotate(a);
			oeil4.Rotate(a);
			nez.Rotate(a);
			bouche.Rotate(a);
			joue1.Rotate(a);
			joue2.Rotate(a);
			oreille1.Rotate(a);
			oreille2.Rotate(a);

		}
		Point Center() { return center; }
		int getType() { return 22; }
		void setColor(Color couleur) override { 
			tete.setColor(couleur);
			oreille1.setColor(couleur);
			oreille2.setColor(couleur);
		}
		friend std::ostream& operator<<(std::ostream& s, const Pikachu& c);
		friend SVG::stream& operator<<(SVG::stream& s, const Pikachu& c);
		};




		////////////////////////////////////////////////////////////////////////////////////////
		//  inline definitions

		// polygon
		inline Polygon::Polygon(std::size_t npts, const Point &cent, float size, const Color &color) :
			Base(color), Array::Fixed<Point>(npts) {
				Array::Geometry::FillnGon(*this,cent,size);
		}
		inline Polygon::Polygon(const std::initializer_list<Point> &list, const Color &color) :
			Base(color), Array::Fixed<Point>(list) {}
	}
}

#endif // _SURFACE_H_

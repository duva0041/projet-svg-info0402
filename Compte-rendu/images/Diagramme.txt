@startuml
class Transform {
  translate()
  rotate()
  Scale()
}

Transform <|-- Geom2d
Geom2d <|-- Surface
Fill <|-- Surface

Surface <|-- Disk
Surface <|-- Polygon
Surface <|-- Rectangle
Surface <|-- Pentagone
Surface <|-- Heptagone
Surface <|-- Hexagone
Surface <|-- Octogone
Surface <|-- Losange
Surface <|-- Parallélogramme
Surface <|-- Pikachu
Surface <|-- Coeur
Surface <|-- Fleur
Surface <|-- Etoile
Surface <|-- Sun
Surface <|-- Snowflake
DynamicArray<|-- Figure
SVG <|-- Surface
Triangle : StaticArray<Points,3>
Pentagone : StaticArray<Points,5>
Heptagone : StaticArray<Points,7>
Hexagone : StaticArray<Points,6>
Octogone : StaticArray<Points,8>
Etoile : StaticArray<Points,10>
SnowFlake : StaticArray<Points,12>
Sun : StaticArray<Points,36>
Parallelogramme : StaticArray<Points,12>

class Grawink{
}

Surface <|-- Triangle


class Rectangle {
  Point
  Width
  Height
}

class Surface {
}

class Figure {
  DynamicArray<Geom2D>
  Add(Geom2D)
  Remove(Geom2D)
}
class SVG {}
Transform <|-- Figure
Grawink <|-- Figure

class StaticArray<class T, size_t n> {
  T tab<N>
}
class FixedArray<class T> {
  TArray
  Size
}
class DynamicArray<class T> {
  Allocated
}
class Point{}
Point<|-- Transform
FixedArray <|-- DynamicArray

PointsArray : DynamicArray<Points>
Polygon : DynamicArray<Points>

Disk : Point
Disk : radius

class Curve{}
Geom2d<|-- Curve
class Ellipse{}
class Circle{}
class Polyline{}
class Segment{}
Curve<|-- Segment
Curve<|-- Ellipse
Curve<|-- Circle
Curve<|-- Polyline
SVG<|--Curve
PointsArray<|--Curve

Rectangle <|-- Square
Transform <|-- PointsArray
@enduml